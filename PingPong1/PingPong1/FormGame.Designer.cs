﻿namespace PingPong1
{
    partial class FormGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Bol = new System.Windows.Forms.Panel();
            this.timerGo = new System.Windows.Forms.Timer(this.components);
            this.User1 = new System.Windows.Forms.Panel();
            this.User2 = new System.Windows.Forms.Panel();
            this.timerChak = new System.Windows.Forms.Timer(this.components);
            this.BoxPrint = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Bol
            // 
            this.Bol.BackColor = System.Drawing.Color.Green;
            this.Bol.Location = new System.Drawing.Point(280, 305);
            this.Bol.Name = "Bol";
            this.Bol.Size = new System.Drawing.Size(25, 25);
            this.Bol.TabIndex = 0;
            // 
            // timerGo
            // 
            this.timerGo.Interval = 10;
            this.timerGo.Tick += new System.EventHandler(this.timerGo_Tick);
            // 
            // User1
            // 
            this.User1.BackColor = System.Drawing.Color.Blue;
            this.User1.Location = new System.Drawing.Point(20, 245);
            this.User1.Name = "User1";
            this.User1.Size = new System.Drawing.Size(20, 120);
            this.User1.TabIndex = 1;
            this.User1.Tag = "User";
            // 
            // User2
            // 
            this.User2.BackColor = System.Drawing.Color.Red;
            this.User2.Location = new System.Drawing.Point(860, 245);
            this.User2.Name = "User2";
            this.User2.Size = new System.Drawing.Size(20, 120);
            this.User2.TabIndex = 2;
            this.User2.Tag = "User";
            // 
            // timerChak
            // 
            this.timerChak.Interval = 1;
            this.timerChak.Tick += new System.EventHandler(this.timerChak_Tick);
            // 
            // BoxPrint
            // 
            this.BoxPrint.AutoSize = true;
            this.BoxPrint.Location = new System.Drawing.Point(17, 9);
            this.BoxPrint.Name = "BoxPrint";
            this.BoxPrint.Size = new System.Drawing.Size(35, 13);
            this.BoxPrint.TabIndex = 3;
            this.BoxPrint.Text = "label1";
            this.BoxPrint.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(899, 694);
            this.Controls.Add(this.BoxPrint);
            this.Controls.Add(this.User2);
            this.Controls.Add(this.User1);
            this.Controls.Add(this.Bol);
            this.Name = "Form1";
            this.Text = "פינג פונג";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.SizeChanged += new System.EventHandler(this.Form1_SizeChanged);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyUp);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel Bol;
        private System.Windows.Forms.Timer timerGo;
        private System.Windows.Forms.Panel User1;
        private System.Windows.Forms.Panel User2;
        private System.Windows.Forms.Timer timerChak;
        private System.Windows.Forms.Label BoxPrint;
    }
}

