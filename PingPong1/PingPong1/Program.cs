﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PingPong1
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        //[STAThread]
        static public FormGame fom;
        static public bool stop = true;
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            fom = new FormGame(0, 0);
            fom.ShowDialog();
            while (stop)
            {
                int a = fom.px1,b = fom.px2;
                fom = new FormGame(a, b);
                fom.px1 = a;
                fom.px2 = b;
                fom.ShowDialog();
            }
        }
    }
}
